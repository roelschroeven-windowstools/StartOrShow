//---------------------------------------------------------------------------

#include <vcl.h>
#include <windows.h>
#pragma hdrstop

#pragma link "TrescoUtilLib.lib"

#include <boost/regex.hpp>
#include <TrescoUtilLib.h>

#include <tchar.h>
//---------------------------------------------------------------------------

//
// Try to find window
//

struct TFindWindowContext
{
  boost::wregex TitleFilter;
  HWND hWnd;
};

// Callback for enumerating windows
BOOL CALLBACK EnumWindowsCallback(HWND hWnd, LPARAM lParam)
{
  wchar_t WndTitle[1024];
  GetWindowText(hWnd, WndTitle, ARRAY_SIZE(WndTitle));

  TFindWindowContext *pContext = (TFindWindowContext*)lParam;
  if (regex_match(WndTitle, pContext->TitleFilter))
    {
    // Window found; pass window handle
    pContext->hWnd = hWnd;
    // Return false because we can stop enumerating now
    return FALSE;
    }
  return TRUE;
}

// Start enumerating windows
HWND FindDesiredWindow(const boost::wregex &WindowTitleFilter)
{
  TFindWindowContext Context;
  Context.TitleFilter = WindowTitleFilter;
  Context.hWnd = NULL;
  EnumWindows(EnumWindowsCallback, (LPARAM)&Context);
  return Context.hWnd;
}

//
// Start program
//

void StartDesiredProgram(String ExeFilename)
{
  STARTUPINFO StartupInfo;
  PROCESS_INFORMATION ProcInfo;

  memset(&StartupInfo, 0, sizeof(StartupInfo));
  StartupInfo.cb = sizeof(StartupInfo);

  BOOL rv = CreateProcess(
    ExeFilename.c_str(),
    NULL,
    NULL,
    NULL,
    FALSE,
    0,
    NULL,
    ExtractFilePath(ExeFilename).c_str(),
    &StartupInfo,
    &ProcInfo);
  CloseHandle(ProcInfo.hThread);
  CloseHandle(ProcInfo.hProcess);
}

//
// Show window / bring it to front / activate it / focus it (whatever you want to name it ...)
//

void ShowDesiredWindow(HWND hWnd)
{
  WINDOWPLACEMENT WindowPlacement;
  GetWindowPlacement(hWnd, &WindowPlacement);
  if (WindowPlacement.showCmd == SW_SHOWMAXIMIZED)
    ShowWindow(hWnd, SW_SHOWMAXIMIZED);
  else
    ShowWindow(hWnd, SW_RESTORE);

  SetForegroundWindow(hWnd);
}

#pragma argsused
WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
  if (__argc < 3)
    {
    ShowMessage("Not enough arguments");
    return 1;
    }

  HWND hWnd = FindDesiredWindow(boost::wregex(_wargv[1], boost::wregex::perl));
  if (hWnd == NULL)
    StartDesiredProgram(_wargv[2]);
  else
    ShowDesiredWindow(hWnd);
  return 0;
}
//---------------------------------------------------------------------------
